const executeProcedure = require('../config/mssql');
const config = require('../config');
const cron = require('node-cron');
const fs = require('fs');
const DocumentBody = require('../models/DocumentBody');
const TradeMark = require('../models/TradeMark');
const log = require('../config/logger');


class Procedure {

  runSchedule(){
    const documentSchedule = config.get('document:clearInterval');
    const tradeMarkSchedule = config.get('tradeMark:clearInterval');
    let valid = cron.validate(documentSchedule);
    if (!valid) {
      throw new Error('Invalid node-cron interval configuration for document:clearInterval');
    }
    valid = cron.validate(tradeMarkSchedule);
    if (!valid) {
      throw new Error('Invalid node-cron interval configuration for tradeMark:clearInterval');
    }
    cron.schedule(documentSchedule, async () => {
      await DocumentBody.removeOld();
    });
    cron.schedule(tradeMarkSchedule, async () => {
      await TradeMark.removeOld();
    });
  }




  async getTradeMarkByappNum(appNum) {
    let doc = await TradeMark.getByappNum(appNum);
    if (doc) {
      await TradeMark.updateCreatedAtByappNum(appNum);
      return doc;
    } else {
      const buffer = await this.getTradeMarkBody(appNum)
      if (buffer) {
        doc = await TradeMark.add({
          applicationNumber: appNum,
          BlobDocument: buffer,
          BodyLen: buffer.length
        });
        return doc[0];
      }
    }
  }

  async getTradeMarkBody(appNum) {
    const appInfo = await this.Get_ApplicationInfo(appNum);
    const path = this.getLocalFilePath(appInfo);
    if (!path) throw new Error('TradeMark not exist in CEAD')
    const buffer = getBufferByPath(path);
    return buffer;
  }

  /**
   * @method
   * @async 
   * @description процедура для отримання информації по заявці
   * @params {String} appNum - поле applicationNumber моделі Zayavka
   * @returns {Promise<Array>} - проміс, який містить масив даних по заявцs
  */
  async Get_ApplicationInfo(appNum) {
    const { recordset } = await executeProcedure('Get_ApplicationInfo', {ApplicationNumber: appNum});
    return recordset;
  }

  /**
   * @method
   * @description функція для одержання локальных посилань на файли
   * @params {Array<Object>} docs - масив документів з результату процедури Get_ApplicationInfo
   * @returns {String|Null} - строка або null
  */
  getLocalFilePath(docs){
    let basePath = '';
    let appNum = '';
    if (docs.length === 1) {
      basePath = docs[0].LocalFilePath;
      appNum = docs[0].APP_Number;
    } else if (docs.length > 1) {
      const index = docs.findIndex( i => i.RegistrationNumber !== '');
      if (~index) {
        basePath = docs[index].LocalFilePath;
        appNum = docs[index].APP_Number;
      } else {
        return null
      }
    } else {
      return null
    }

    return basePath + appNum + '.jpeg'
  }

  async getDocByidDoc(idDoc) {
    let doc = await DocumentBody.getByidDoc(idDoc);
    if (doc) {
      await DocumentBody.updateCreatedAtByidDoc(idDoc);
      return doc;
    } else {
      doc = await this.Get_DocumentBody(idDoc);
      console.log('doc', doc)
      if (doc) {
        doc.idDoc = idDoc
        await DocumentBody.add(doc);
        return doc;
      }
      throw new Error('Document not exist in CEAD');
    }
  }

  /**
   * @async
   * @method
   * @description Процедура перегляду тіла документа
   * @param {Object} inputParams 
   */
  async Get_DocumentBody(idDoc) {
    const { recordset } = await executeProcedure('Get_DocumentBody', {idDoc: idDoc});
    return recordset[0];
    /*console.log(data)
    if(data) bufferSaveAsFile(data.recordset[0]);*/
  }

  async Get_NotWorkedDocuments() {
    const { recordset } = await executeProcedure('Get_NotWorkedDocuments');
    return recordset;
  }


  async exp_getlist_newdocuments(dateFrom, is_firstDoc){
    const { recordset } = await executeProcedure('exp_getlist_newdocuments', {DateFrom: dateFrom, is_firstDoc: is_firstDoc});
    return recordset;
  }

}


/**
 * @function
 * @description запис данных в JSON форматі у файли
 */
function saveAsJson(data, name){
  const file = `${__dirname}/../public/json/${name}.json`;

  const json = JSON.stringify(data, null, '\t');
  
  fs.writeFile(file, json, function(err, doc){
    if(err) throw err;
  });
}

function bufferSaveAsFile(data){
  const { id, BlobDocument, BlobType } = data;
  const file = `${__dirname}/../public/json/${id}_${Date.now()}.${BlobType ? BlobType : 'pdf'}`;
  fs.writeFile(file, BlobDocument, function(err, doc){
    if(err) throw err;
  });
}



/**
 * @description отримати буфер по шляху до файлу
 * 
 * @param {String} path - шлях до файлу у файловій системі 
 * @returns {Promise<Buffer>} - Проміс з данними у виді буфера
 */
function getBufferByPath(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, function(err, data) {
      if (err) {
          reject(err)
      }
      resolve(data);
    });
  });
}

module.exports = new Procedure();