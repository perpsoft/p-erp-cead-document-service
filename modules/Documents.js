const Procedure = require('./Procedure');
const config = require('../config');
const fs = require('fs');
const fetch = require('node-fetch');

const port = process.env.PORT || config.get('port');
const NotWorkedDocuments = `http://localhost:${ port }/static/json/NotWorkedDocuments.json`;

const output = `${__dirname}/../../../documents_pdf/`

const getAll = async function(){
  const json = await getJson(NotWorkedDocuments)
  
    // групировка по idDoc  //17511589, 17497904, 17477959 fail

    // end 17477681
    const docs = new Set();
    for (const doc of json) {
      const { idObjType, idDoc } = doc;
      if (idObjType === 2 &&  idDoc < 17477959){
        docs.add(idDoc);
      } 

    }
    console.log('size: ', docs.size)

    for (const item of docs) {
      console.log('start', item)
      try {
        const doc = await Procedure.Get_DocumentBody(item);
        if (doc && doc.isScaned) {
          const ext = 'pdf';
          const path = `${output}${item}.${ext}`;
          await saveAsFile(doc.BlobDocument, path);
          console.log('ok', item)
        } else {
          console.log('skip', item)
        }
      } catch(e) {
        console.log('error', item, e.message)
      }
    }


  /*  const id = 20095192;
    const doc = await Procedure.Get_DocumentBody(id);
    console.log(doc)
    const path = `${output}${doc.id}.pdf`;
    await saveAsFile(doc.BlobDocument, path)
*/
    

}

function getJson(url){
  console.log('getjson', url)
  return fetch(url)
    .then(res => res.json())
    .catch(e => console.log(e))
}

const saveAsFile = function(data, path){
  //console.log('saveAsFile')
  return new Promise((resolve, reject) => {
      fs.writeFile(path, data, function(err, doc){
          if (err) {
              reject(err)
          }
          resolve(true);
      });
  });
}


module.exports = getAll;