const config = require('../config');
const mongoose = require('mongoose');
const log = require('./logger');
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

const uri = config.get('mongoose:uri');
const options = config.get('mongoose:options');


mongoose.connectionPending = mongoose.createConnection(uri, options)
  /*.then((connection) => {
    return connection.dropDatabase();
  })*/
  .then(() => mongoose.connect(uri, options))
  .then(() => log.info({action: 'MONGOOSE CONNECT'}))
  .catch(err => {
    log.error({action: 'MONGOOSE CONNECT', message: err.message});
    console.log(err)
  });

module.exports = mongoose;