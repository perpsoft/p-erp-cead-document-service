const fetch = require('node-fetch');
const mongoose = require('mongoose');
const Procedure = require('../modules/Procedure');
const getAll = require('../modules/Documents');
const DocumentBody = require('../models/DocumentBody');
const log = require('./logger');
const config = require('./index');
const cron = require('node-cron');


Date.prototype.addHours = function (h){
  this.setHours(this.getHours()+h);
  return this;
};

Array.prototype.isEmpty = function () {
  return !this.length;
};

Array.prototype.onlyUnique = function () {
  return [...new Set(this)];
};

JSON.isJsonString = function (str) {
try {
  JSON.parse(str);
} catch (e) {
  return false;
}
return true;
};


const initApp = async function () {
  try {
    await mongoose.connectionPending;



    Procedure.runSchedule();
   // await getAll();


    console.log('SERVICE STARTED')
    log.info({action: 'SERVICE STARTED'})
  } catch (error) {
    console.log('initApp_error', error)
    log.error({action: 'SERVICE STARTED', error: error.message})
  }
};



module.exports = initApp;