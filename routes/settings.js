const express = require('express');
const router = express.Router();
const log = require('../config/logger');
const Document = require('../models/Document');

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/:id', function(req, res, next) {
  const id = req.params.id;
  Document.getById(id)
    .then((docs) => {
      console.log('docs', docs.type)
      res.json({status: "ok", docs})
    })
    .catch(() => {
      res.status(400).json({error: "Bad request"});
    });

 // res.render('index', { title: 'Express' });
});

/**
 * @api {post} settings/:settings зміна налаштувань сервісу
 * @apiVersion 0.1.0
 * @apiName setSettings
 * @apiGroup Settings
 *
 * @apiParamExample {json} Request-Example:
 * {
 *  "testfield":"",
 *  }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "ok",
 *       text: "settings changed"
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Not Found
 *     {
 *       "error": "Bad request",
 *     }
 */

router.put('/', function (req, res) {
  log.info({'action': 'SETTINGS CHANGED'});
  res.status(200).json(
      {
        status: "ok",
        text: "settings changed"
      }
    );
});

module.exports = router;
