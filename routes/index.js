const express = require('express');
const router = express.Router();
const Procedure = require('../modules/Procedure')
const config = require('../config')

router.get('/', function(req, res, next) {
  res.render('index', { title: `${config.get('X-Service-Name')} ${config.get('X-Service-Version')}`});
});

router.get('/document/:id', async function(req, res, next) {
  const id = req.params.id;
  try {
    const doc = await Procedure.getDocByidDoc(id);
    res.writeHead(200, {
      'Content-Type': 'application/pdf; charset=utf-8',
      //'Content-Disposition': `attachment; filename=document_${id}.pdf`,
      'Content-Length': doc.BodyLen
    });
    res.end(doc.BlobDocument);
  } catch (error) {
    res.status(400).json({error: error.message});
  }
});

router.get('/trademark/:appNum', async function(req, res, next) {
  const appNum = req.params.appNum;
  try {
    const doc = await Procedure.getTradeMarkByappNum(appNum);
    res.writeHead(200, {
      'Content-Type': 'image/jpeg; charset=utf-8',
      //'Content-Disposition': `attachment; filename=trademark_${appNum}.jpeg`,
      'Content-Length': doc.BodyLen
    });
    res.end(doc.BlobDocument);
  } catch (error) {
    res.status(400).json({error: error.message});
  }
});


module.exports = router;