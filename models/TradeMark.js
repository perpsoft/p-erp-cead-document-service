/**
 * @namespace
 * @name Models
 * @description Класи моделей
 */
const config = require('../config');
const mongoose = require('../config/mongoose');
const Schema = mongoose.Schema;


/**
 * @class Request
 * @name Models.TradeMark
 * @description модель заявок
 * @memberOf Models
 */

const schema = new Schema({
  applicationNumber: {
    type: String,
    required: true,
    unique: true
  },
  BlobDocument: Schema.Types.Buffer,
  BodyLen: Number,
  createdAt: {
    type: Number,
    default: Date.now
  }
});




/**
 * @alias TradeMark.prototype.add
 * @name Models.TradeMark.add
 *
 * @method
 * @async
 * @description Метод додає документ або масив документів в БД
 * @param {Object|Array<Object>} docs - Об'єкт документа АБО масив документів
 * @param {Object} idDoc - Ідентифікатор фактичного документа (model Document.idDoc)
 * @param {Number} id - Ідентифікатор
 * @param	{Buffer} BlobDocument	- Тіло документа
 * @param {String|Null} BlobType - Тип тіла документа (doc,pdf…) Може бути NULL
 * @param {Number} BodyLen - Довжина BLOB
 * @param {Boolean} isScaned - ознака що відскановано


 * 
 * @example [
    { 
      idDoc: 17342527,
      id: 19887823,
      BlobDocument: '<Buffer 25 50 44 46 2d ...>'
      BlobType: null,
      BodyLen: 160252,
      isScaned: 1 
    },
    ...
    ]
 *   
 * @returns {Promise<Array>} - проміс, який містить массив об'єктів доданих документыв згідно схеми моделі
 *
 */
schema.statics.add = async function (docs) {
  const docList = Array.isArray(docs) ? docs : [docs];
  return await this.insertMany(docList);
}

/**
 * @alias TradeMark.prototype.getByappNum
 * @name Models.TradeMark.getUByappNum
 *
 * @async
 * @method
 * @description Отримання торгового знака по applicationNumber
 * @param {Number} appNum - applicationNumber документа
 *
 * @returns {Promise<Object>} - проміс, який містить об'єкт документа 
 */
schema.statics.getByappNum = async function (appNum) {
  return await this.findOne({applicationNumber: appNum});
};

/**
 * @alias TradeMark.prototype.updateCreatedAtByappNum
 * @name Models.TradeMark.updateCreatedAtByappNum
 *
 * @method
 * @async
 * @description Метод обновлюе дату створення запису по applicationNumber
 * @param {Number} appNum - applicationNumber документа
 *
 * @returns {Promise<Object>} - проміс, який містить об'єкт документа 
 */
schema.statics.updateCreatedAtByappNum = async function (appNum) {
  return await this.findOneAndUpdate({applicationNumber: appNum}, {createdAt: Date.now()});
};



/**
 * @alias TradeMark.prototype.removeOld
 * @name Models.TradeMark.removeOld
 *
 * @method
 * @async
 * @description Метод видяляе всі устарілі документи з БД 
 *
 * @returns {Promise<Object>} - проміс, який містить інформацію про кількість видаленніх докуменів
 */
schema.statics.removeOld = async function () {
  const cacheTime = config.get('tradeMark:cacheTime');
  if (cacheTime === 0) {
    return; // не видаляемо
  } 
  const maxAge = cacheTime || 3600 * 24 // за замовчуванням 24 години
  const exp = `this.createdAt + ${ maxAge } * 1000 < Date.now()`;
  return await this.deleteMany({$where: exp});
};


/**
 * @alias TradeMark.prototype.exists
 * @name Models.TradeMark.exists
 *
 * @method
 * @async
 * @description Метод шукае запис у базі по заданому фільтру 
 *
 * @returns {Promise<Boolean>} - проміс, true - якщо запис існує
 */
schema.statics.exists = async function (filter = {}){
  return await this.findOne(filter).select({ _id: 1 }).lean().then(doc => !!doc);
}

const TradeMark = mongoose.model('TradeMark', schema);

module.exports = TradeMark;