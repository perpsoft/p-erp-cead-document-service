/**
 * @namespace
 * @name Models
 * @description Класи моделей
 */
const config = require('../config');
const mongoose = require('../config/mongoose');
const Schema = mongoose.Schema;

/**
 * @class Request
 * @name Models.DocumentBody
 * @description модель заявок
 * @memberOf Models
 */

const schema = new Schema({
  idDoc: {
    type: Number,
    required: true,
    unique: true
  },
  id: {
    type: Number,
    required: true,
    unique: true
  },
  BlobDocument: Schema.Types.Buffer,
  BlobType: String,
  BodyLen: Number,
  isScaned: Boolean,
  createdAt: {
    type: Number,
    default: Date.now
  }

});

/* { 
  id: 19887823,
  BlobDocument:
  <Buffer 25 50 44 46 2d ...>
  BlobType: null,
  BodyLen: 160252,
  isScaned: 1 
} */


/**
 * @alias DocumentBody.prototype.add
 * @name Models.DocumentBody.add
 *
 * @method
 * @async
 * @description Метод додає документ або масив документів в БД
 * @param {Object|Array<Object>} docs - Об'єкт документа АБО масив документів
 * @param {Object} idDoc - Ідентифікатор фактичного документа (model Document.idDoc)
 * @param {Number} id - Ідентифікатор
 * @param	{Buffer} BlobDocument	- Тіло документа
 * @param {String|Null} BlobType - Тип тіла документа (doc,pdf…) Може бути NULL
 * @param {Number} BodyLen - Довжина BLOB
 * @param {Boolean} isScaned - ознака що відскановано


 * 
 * @example [
    { 
      idDoc: 17342527,
      id: 19887823,
      BlobDocument: '<Buffer 25 50 44 46 2d ...>'
      BlobType: null,
      BodyLen: 160252,
      isScaned: 1 
    },
    ...
    ]
 *   
 * @returns {Promise<Array>} - проміс, який містить массив об'єктів доданих документыв згідно схеми моделі
 *
 */
schema.statics.add = async function (docs) {
  const docList = Array.isArray(docs) ? docs : [docs];
  return await this.insertMany(docList);
}

/**
 * @alias DocumentBody.prototype.getByidDoc
 * @name Models.DocumentBody.getUByidDoc
 *
 * @async
 * @method
 * @description Отримання документа по idDoc
 * @param {Number} idDoc - idDoc документа
 *
 * @returns {Promise<Object>} - проміс, який містить об'єкт документа 
 */
schema.statics.getByidDoc = async function (idDoc) {
  return await this.findOne({idDoc: idDoc});
};

/**
 * @alias DocumentBody.prototype.updateCreatedAtByidDoc
 * @name Models.DocumentBody.updateCreatedAtByidDoc
 *
 * @method
 * @async
 * @description Метод обновлюе дату створення запису по idDoc
 * @param {Number} idDoc - idDoc документа
 *
 * @returns {Promise<Object>} - проміс, який містить об'єкт документа 
 */
schema.statics.updateCreatedAtByidDoc = async function (idDoc) {
  return await this.findOneAndUpdate({idDoc: idDoc}, {createdAt: Date.now()});
};



/**
 * @alias DocumentBody.prototype.removeOld
 * @name Models.DocumentBody.removeOld
 *
 * @method
 * @async
 * @description Метод видяляе всі устарілі документи з БД 
 *
 * @returns {Promise<Object>} - проміс, який містить інформацію про кількість видаленніх докуменів
 */
schema.statics.removeOld = async function () {
  const cacheTime = config.get('document:cacheTime');
  if (cacheTime === 0) {
    return; // не видаляемо
  } 
  const maxAge = cacheTime || 3600 * 24 // за замовчуванням 24 години
  const exp = `this.createdAt + ${ maxAge } * 1000 < Date.now()`;
  return await this.deleteMany({$where: exp});
};


/**
 * @alias DocumentBody.prototype.exists
 * @name Models.DocumentBody.exists
 *
 * @method
 * @async
 * @description Метод шукае запис у базі по заданому фільтру 
 *
 * @returns {Promise<Boolean>} - проміс, true - якщо запис існує
 */
schema.statics.exists = async function (filter = {}){
  return await this.findOne(filter).select({ _id: 1 }).lean().then(doc => !!doc);
}

const DocumentBody = mongoose.model('DocumentBody', schema);

module.exports = DocumentBody;